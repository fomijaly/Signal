import { Component } from '@angular/core';
import { DescriptifComponent } from '../../shared/descriptif/descriptif.component';
import { FeaturesComponent } from '../../shared/features/features.component';
import { MainHeroComponent } from '../../shared/main-hero/main-hero.component';
import { MainInformationComponent } from '../../shared/main-information/main-information.component';
import { LeftInformationComponent } from '../../shared/left-information/left-information.component';
import { RightInformationComponent } from '../../shared/right-information/right-information.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    MainHeroComponent,
    DescriptifComponent,
    MainInformationComponent,
    FeaturesComponent,
    LeftInformationComponent,
    RightInformationComponent
  ],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {

}
