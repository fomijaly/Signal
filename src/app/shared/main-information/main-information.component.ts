import { Component } from '@angular/core';

@Component({
  selector: 'app-main-information',
  standalone: true,
  imports: [],
  templateUrl: './main-information.component.html',
  styleUrl: './main-information.component.css'
})
export class MainInformationComponent {
  title = "Partager sans insécurité";
  description = "Un chiffrement de bout en bout de pointe (propulsé par le protocole Signal à code source ouvert) assure la sécurité de vos conversations. Nous ne pouvons ni lire vos messages ni écouter vos appels, et personne d’autre que vous ne peut le faire. La confidentialité n’est pas proposée en option, c’est simplement la façon dont Signal fonctionne. Pour tous les messages, pour tous les appels, tout le temps.";
  img = "../../../assets/animation.png";
}
