import { Component } from '@angular/core';
import { FullButtonComponent } from '../buttons/full-button/full-button.component';

@Component({
  selector: 'app-main-hero',
  standalone: true,
  imports: [
    FullButtonComponent
  ],
  templateUrl: './main-hero.component.html',
  styleUrl: './main-hero.component.css'
})
export class MainHeroComponent {

}
