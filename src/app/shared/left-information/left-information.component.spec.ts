import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftInformationComponent } from './left-information.component';

describe('LeftInformationComponent', () => {
  let component: LeftInformationComponent;
  let fixture: ComponentFixture<LeftInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LeftInformationComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(LeftInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
