import { Component } from '@angular/core';

@Component({
  selector: 'app-left-information',
  standalone: true,
  imports: [],
  templateUrl: './left-information.component.html',
  styleUrl: './left-information.component.css'
})
export class LeftInformationComponent {
  title = "Aucune publicité, aucun traqueur, vraiment.";
  description = "Dans Signal, il n’y a aucune publicité, aucun vendeur partenaire, ni aucun système de suivi inquiétant. Vous pouvez vous consacrer à partager les moments importants avec les personnes qui comptent pour vous.";
  img = "../../../assets/No-Ads.png";
}
