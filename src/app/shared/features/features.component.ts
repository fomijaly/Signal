import { Component } from '@angular/core';
import { FeatureBoxComponent } from '../feature-box/feature-box.component';

export type Box = {
  id: number,
  img: string,
  title: string,
  description: string
}
@Component({
  selector: 'app-features',
  standalone: true,
  imports: [FeatureBoxComponent],
  templateUrl: './features.component.html',
  styleUrl: './features.component.css'
})

export class FeaturesComponent {

  ngOnInit(): void{
    fetch('../assets/features.json')
    .then((data) => data.json())
    .then((response: Box[]) => this.featuresBoxes = response)
    .catch((error) => console.error(error))
  }

  featuresBoxes : Box[] = [];
}
