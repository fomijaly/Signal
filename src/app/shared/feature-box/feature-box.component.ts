import { Component, Input } from '@angular/core';
import { Box } from '../features/features.component';

@Component({
  selector: 'app-feature-box',
  standalone: true,
  imports: [],
  templateUrl: './feature-box.component.html',
  styleUrl: './feature-box.component.css'
})

export class FeatureBoxComponent {
  @Input()
  box? :  Box
}
