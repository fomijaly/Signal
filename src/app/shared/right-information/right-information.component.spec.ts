import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RightInformationComponent } from './right-information.component';

describe('RightInformationComponent', () => {
  let component: RightInformationComponent;
  let fixture: ComponentFixture<RightInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RightInformationComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RightInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
