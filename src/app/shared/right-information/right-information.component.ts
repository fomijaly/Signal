import { Component } from '@angular/core';
import { EmptyButtonComponent } from '../buttons/empty-button/empty-button.component';

@Component({
  selector: 'app-right-information',
  standalone: true,
  imports: [EmptyButtonComponent],
  templateUrl: './right-information.component.html',
  styleUrl: './right-information.component.css'
})
export class RightInformationComponent {
  title = "Gratuit pour tous";
  description = "Signal est un organisme à but non lucratif indépendant. Nous ne sommes reliés à aucune entreprise technologique importante et nous ne pourrons jamais être achetés par l’une d’elles. Le développement de notre plateforme est financé par des subventions et des dons de personnes comme vous.";
  img = "../../../assets/Nonprofit503.png";
}
