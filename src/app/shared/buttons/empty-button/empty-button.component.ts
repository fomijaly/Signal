import { Component } from '@angular/core';

@Component({
  selector: 'app-empty-button',
  standalone: true,
  imports: [],
  templateUrl: './empty-button.component.html',
  styleUrl: './empty-button.component.css'
})
export class EmptyButtonComponent {
  titre = "Faire un don à Signal"
}
